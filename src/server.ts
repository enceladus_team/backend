import http from 'http';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';

import { Config } from './config';

const createHttpServer = async (config: Config, registerRoutes: (app: any) => void) => {
  const app = express();
  const server = http.createServer(app);

  app.use(morgan('dev'));
  app.use(cors({origin: '*'}));
  app.use(bodyParser.json({
    limit: config.bodyLimit,
  }));
  app.use(bodyParser.urlencoded({
    extended: true,
  }));

  registerRoutes(app);

  return server;
};

export default createHttpServer;