import config from './config';
import createHttpServer from './server';
import registerRoutes from './routes';
import logger from './utils/logger';

async function initServer() {
  const server = await createHttpServer(config, registerRoutes);
    // const io = socket(server, {origins: '*'});

  server.listen(config.port, () => {
    logger.info('Server : Online');
    logger.info(`Port : ${config.port}`);
  });
}

initServer()