import { Response, Request } from 'express';

export const helloWorld = (_: Request, res: Response) => {
  res.send('Hello World');
};
