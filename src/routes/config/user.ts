import { helloWorld } from '../user';

const userRoutes = {
  helloWorld: {
    callback: helloWorld,
    path: '/',
    method: 'get',
    middleware: [],
  },
};

export default userRoutes;
