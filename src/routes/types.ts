import { Request, Response } from 'express';

export type RouteCallback = (req: Request, res: Response) => {
  status: number,
  body: any,
};

export interface Route {
  callback: RouteCallback;
  path: string;
  method: 'get' | 'post';
  middleware?: () => void;
}
