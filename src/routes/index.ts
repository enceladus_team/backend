import { Request, Response } from 'express';
import status from 'http-status-codes';

import logger from '../utils/logger';
import userRoutes from './config/user';
import { Route } from './types';

const routes = {
  ...userRoutes,
};

const routeHandler = (name: string, route: Route) => async (req: Request, res: Response) => {
  try {
    const result = await route.callback(req, res);

    if (typeof result === 'undefined') {
      return;
    }

    res.status(result.status).json(result.body);

  } catch (err) {
    logger.error(`Failed to handle route ${name} at ${route.method} ${route.path}`);
    logger.error(err);

    res.status(status.INTERNAL_SERVER_ERROR);
  }
};

const registerRoutes = (app: any) => {
  const routeNames = Object.keys(routes);

  for (const name of routeNames) {
    const route = routes[name];
    const method = route.method;
    const register = app[method];
    const middleware = route.middleware || [];
    const handlers = [...middleware, routeHandler(name, route)];

    register.call(app, route.path, ...handlers);
  }
};

export default registerRoutes;
