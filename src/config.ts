const config = {
  port: process.env.PORT || 3000,
  bodyLimit: '100kb',
};

export type Config = typeof config;

export default config;
