module.exports = {
    "presets": [
      [
        "@babel/preset-env",
        {
          "targets": {
            "node": "8"
          }
        }
      ],
      "@babel/typescript"
    ],
    "plugins": [
      "@babel/proposal-class-properties",
      "@babel/proposal-object-rest-spread",
    ]
  }